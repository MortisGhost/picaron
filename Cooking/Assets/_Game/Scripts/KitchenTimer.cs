using MoreMountains.Feedbacks;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class KitchenTimer : MonoBehaviour
{
    [Header("Time")]
    [SerializeField] private float duration;

    [Header("Feedback")]
    [SerializeField] private MMF_Player endFeedback;

    [Header("Broadcasting On")]
    [SerializeField] private BoolEventChannelSO onTimeFinish;

    [Header("Event")]
    [SerializeField] private UnityEvent onFinish;

    private Coroutine _timeRoutine;

    public void StartTimer()
    {
        _timeRoutine = StartCoroutine(TimerRoutine());
    }

    public void StopTimer()
    {
        if (_timeRoutine != null)
        {
            StopCoroutine(_timeRoutine);
        }
    }

    private IEnumerator TimerRoutine()
    {
        yield return new WaitForSeconds(duration);
        endFeedback.PlayFeedbacks();

        onTimeFinish.RaiseEvent(true);
        onFinish.Invoke();
    }
}
