using MoreMountains.Feedbacks;
using System.Collections;
using UnityEngine;

public class BubbleSpawner : MonoBehaviour
{
    [Header("Positions")]
    [SerializeField] private Transform[] bubblePositions;

    [Header("Spawn Timer")]
    [SerializeField] private float spawnTime = 3f;

    [Header("Random")]
    [SerializeField] private bool randomTime;

    [Header("Prefab")]
    [SerializeField] private GameObject[] bubblePrefab;

    [Header("Broadcasting On")]
    [SerializeField] private VoidEventChannelSO onAddPoint;

    [Header("Listening to")]
    [SerializeField] private BoolEventChannelSO onTimeFinish;

    private bool _finished;

    private int randomNumber;
    private int lastNumber;
    private int maxAttempts = 10;

    private float _currentSpawnTime;
    private float _spawnTime;

    private Coroutine _spawnRoutine;

    private void OnEnable()
    {
        onTimeFinish.OnEventRaised += TimeUp;
    }

    private void OnDisable()
    {
        onTimeFinish.OnEventRaised -= TimeUp;
    }

    private void Start()
    {
        _currentSpawnTime = spawnTime;
    }

    private void TimeUp(bool value)
    {
        _finished = value;
    }

    public void StartSpawn()
    {
        _spawnRoutine = StartCoroutine(SpawnRoutine());
    }

    public void StopSpawn()
    {
        if(_spawnRoutine != null)
        {
            StopCoroutine(_spawnRoutine);
        }     
    }

    public void ForceSpawn()
    {
        var randomNumber = Random.Range(1, 3);
        for (int i = 0; i < randomNumber; i++)
        {
            onAddPoint.RaiseEvent();
            SpawnBubble();
        }
    }

    private void SpawnBubble()
    {
        for (int i = 0; randomNumber == lastNumber && i < maxAttempts; i++)
        {
            randomNumber = Random.Range(0, bubblePositions.Length);
        }

        lastNumber = randomNumber;

        var randomPrefab = Random.Range(0, bubblePrefab.Length);
        Instantiate(bubblePrefab[randomPrefab], bubblePositions[randomNumber].position, Quaternion.identity);
    }

    private IEnumerator SpawnRoutine() 
    {
        while (!_finished) 
        {                   
            if (_spawnTime >= _currentSpawnTime)
            {
                _spawnTime = 0f;

                if (randomTime)
                {
                    _currentSpawnTime = Random.Range(0.3f, spawnTime);
                }
                else
                {
                    _currentSpawnTime = spawnTime;
                }

                onAddPoint.RaiseEvent();
                SpawnBubble();
            }
            else
            {
                _spawnTime += Time.deltaTime;
            }

            yield return null;
        }
    }
}
