using MoreMountains.Feedbacks;
using System.Collections;
using UnityEngine;

public class FoodTemperature : MonoBehaviour
{
    [Header("Time")]
    [SerializeField] private float burnTime = 3f;
    [SerializeField] private float timeThreshold = 2.5f;

    [Header("Feedback")]
    [SerializeField] private MMF_Player burnFeedback1;
    [SerializeField] private MMF_Player burnFeedback2;

    [Header("Listening to")]
    [SerializeField] private BoolEventChannelSO onTimeFinish;

    private float _time;

    private bool _playedFeedback;
    private bool _finished;

    private Coroutine _burnRoutine;

    private void OnEnable()
    {
        onTimeFinish.OnEventRaised += TimeUp;
    }

    private void OnDisable()
    {
        onTimeFinish.OnEventRaised -= TimeUp;
    }

    public void StartTimer()
    {
        _burnRoutine = StartCoroutine(TimerRoutine());
    }

    private void TimeUp(bool value)
    {
        _finished = value;
    }

    public void StopTimer()
    {
        if (_burnRoutine != null)
        {
            StopCoroutine(_burnRoutine);
        }

        _playedFeedback = false;
        _time = 0f;
    }

    IEnumerator TimerRoutine()
    {
        while (!_finished)
        {
            if (_time >= burnTime)
            {
                _time = 0f;
                _playedFeedback = false;

                burnFeedback2.PlayFeedbacks();
            }
            else
            {
                _time += Time.deltaTime;
                if (_time >= timeThreshold && !_playedFeedback)
                {
                    burnFeedback1.PlayFeedbacks();
                    _playedFeedback = true;
                }

            }

            yield return null;
        }
    }
}
