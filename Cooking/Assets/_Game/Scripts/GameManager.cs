using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    [Header("Listening to")]
    [SerializeField] private VoidEventChannelSO onAddPoint;
    [SerializeField] private VoidEventChannelSO onSubtractPoint;

    [SerializeField] private int maxBubbles = 10;
    [SerializeField] private UnityEvent onGameOver;

   private int points;

    private void OnEnable()
    {
        onAddPoint.OnEventRaised += AddPoints;
        onSubtractPoint.OnEventRaised += SubtractPoints;
    }

    private void OnDisable()
    {
        onAddPoint.OnEventRaised -= AddPoints;
        onSubtractPoint.OnEventRaised -= SubtractPoints;
    }

    private void AddPoints()
    {
        points++;
        if (points >= maxBubbles)
        {
            onGameOver.Invoke();
        }
    }

    private void SubtractPoints()
    {
        points--;
        if (points <= 0)
        {
            points = 0;
        }
    }
}
