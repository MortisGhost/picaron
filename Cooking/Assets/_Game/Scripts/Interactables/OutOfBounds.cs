using UnityEngine;

public class OutOfBounds : MonoBehaviour
{
    [Header("Broadcasting On")]
    [SerializeField] private VoidEventChannelSO onSubtractPoint;

    private void OnBecameInvisible()
    {
        onSubtractPoint.RaiseEvent();
        gameObject.SetActive(false);
    }
}
