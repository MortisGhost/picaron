using MoreMountains.Feedbacks;
using UnityEngine;
using UnityEngine.Events;

public class PanDrag : MonoBehaviour, IMouseEvents
{
    [Header("Bounds")]
    [SerializeField] private Collider2D _bounds;

    [Header("Feedbacks")]
    [SerializeField] private MMF_Player pickupFeedback;
    [SerializeField] private MMF_Player dropFeedback;

    [Header("Events")]
    [SerializeField] private UnityEvent onPickupEvent;
    [SerializeField] private UnityEvent onDropEvent;

    private Vector2 _screenBounds;
    private Vector2 _offset;

    private float _objectWidth;
    private float _objectHeight;

    private void Start()
    {
        _screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        _objectWidth = _bounds.bounds.extents.x;
        _objectHeight = _bounds.bounds.extents.y;
    }

    Vector2 GetMousePosition()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    public void OnMouseDown()
    {
        if (!enabled)
        {
            return;
        }

        _offset = GetMousePosition() - (Vector2)transform.position;
        pickupFeedback.PlayFeedbacks();
        onPickupEvent.Invoke();
    }

    public void OnMouseDrag()
    {
        if (!enabled)
        {
            return;
        }

        var newPos = GetMousePosition() - _offset;
        newPos.x = Mathf.Clamp(newPos.x, -_screenBounds.x + _objectWidth, _screenBounds.x - _objectWidth);
        newPos.y = Mathf.Clamp(newPos.y, -_screenBounds.y + _objectHeight, _screenBounds.y - _objectHeight);

        transform.position = new Vector3(newPos.x, transform.position.y, 0);
    }

    public void OnMouseUp()
    {
        if (!enabled)
        {
            return;
        }

        dropFeedback.PlayFeedbacks();
        onDropEvent.Invoke();
    }
}
