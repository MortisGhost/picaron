
public interface IMouse
{
    void OnMouseDown();
    void OnMouseUp();
}
