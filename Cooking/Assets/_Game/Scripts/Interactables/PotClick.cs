using MoreMountains.Feedbacks;
using UnityEngine;
using UnityEngine.Events;

public class PotClick : MonoBehaviour, IMouseEvents
{
    [Header("Feedbacks")]
    [SerializeField] private MMF_Player clickFeedback;

    [Header("Events")]
    [SerializeField] private UnityEvent onPickupEvent;
    [SerializeField] private UnityEvent onDropEvent;

    public void OnMouseDown()
    {
        if (!enabled)
        {
            return;
        }

        clickFeedback.PlayFeedbacks();
        onPickupEvent.Invoke();
    }

    public void OnMouseDrag()
    {
        return;
    }

    public void OnMouseUp()
    {
        onDropEvent.Invoke();
    }
}
