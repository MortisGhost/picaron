
public interface IMouseEvents
{
    void OnMouseDown();
    void OnMouseUp();
    void OnMouseDrag();
}
