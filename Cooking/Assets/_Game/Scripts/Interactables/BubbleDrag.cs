using MoreMountains.Feedbacks;
using UnityEngine;

public class BubbleDrag : MonoBehaviour, IMouseEvents
{
    [Header("Drag Speed")]
    [SerializeField] private float mouseDragSpeed = 0.1f;

    [Header("Throw Force")]
    [SerializeField] private float thrust = 1f;

    [Header("Feedbacks")]
    [SerializeField] private MMF_Player pickupFeedback;
    [SerializeField] private MMF_Player dropFeedback;
    [SerializeField] private MMF_Player spawnFeedback;

    private Vector2 _throwVector;
    private Vector2 _offset;

    private Vector3 _dragVelocity = Vector3.zero;

    private Rigidbody2D _body2D;
    private SpriteRenderer _spriteGraphic;

    private void Awake()
    {
        _body2D = GetComponent<Rigidbody2D>();
        _spriteGraphic = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        spawnFeedback.PlayFeedbacks();
    }

    Vector2 GetMousePosition()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    public void OnMouseDown()
    {
        _body2D.velocity = Vector2.zero;

        _spriteGraphic.sortingLayerName = "Above";
        _offset = GetMousePosition() - (Vector2)transform.position;

        pickupFeedback.PlayFeedbacks();
    }

    public void OnMouseDrag()
    {
        var newPos = GetMousePosition() - _offset;
        transform.position = Vector3.SmoothDamp(transform.position, newPos, ref _dragVelocity, mouseDragSpeed);
    }

    public void OnMouseUp()
    {
        _spriteGraphic.sortingLayerName = "Characters";

        var direction = GetMousePosition() - (Vector2)transform.position;
        _throwVector = direction.normalized;

        _body2D.velocity = _throwVector * direction.magnitude * thrust;

        dropFeedback.PlayFeedbacks();
    }
}
