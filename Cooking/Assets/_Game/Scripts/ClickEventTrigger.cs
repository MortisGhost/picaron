using UnityEngine;
using UnityEngine.Events;

public class ClickEventTrigger : MonoBehaviour, IMouseEvents
{
    [SerializeField] private UnityEvent onClickEvent;

    public void OnMouseDown()
    {
        onClickEvent.Invoke();
    }

    public void OnMouseDrag()
    {
        return;
    }

    public void OnMouseUp()
    {
        return;
    }
}
