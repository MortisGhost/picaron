using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// To use a generic UnityEvent type you must override the generic type.
/// </summary>
[System.Serializable]
public class BoolEvent : UnityEvent<bool>
{

}

/// <summary>
/// A flexible handler for int events in the form of a MonoBehaviour. Responses can be connected directly from the Unity Inspector.
/// </summary>
public class BoolEventListener : MonoBehaviour
{
	[SerializeField] private BoolEventChannelSO channel;

	public BoolEvent OnEventRaised;

	private void OnEnable()
	{
		if (channel != null)
		{
			channel.OnEventRaised += Respond;
		}
	}

	private void OnDisable()
	{
		if (channel != null)
		{
			channel.OnEventRaised -= Respond;
		}
	}

	private void Respond(bool value)
	{
		if (OnEventRaised != null)
		{
			OnEventRaised.Invoke(value);
		}
	}
}
