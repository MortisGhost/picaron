﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class is listener for Void Events
/// </summary>

public class VoidEventListener : MonoBehaviour
{
	[SerializeField] private VoidEventChannelSO chanel = default;
	[SerializeField] private UnityEvent OnEventRaised;

	private void OnEnable()
    {
		//Check if the event exists to avoid errors
		if (chanel != null)
		{
			chanel.OnEventRaised += Respond;
		}
	}

	private void OnDisable()
    {
		if (chanel != null)
		{
			chanel.OnEventRaised -= Respond;
		}
	}

    public void Respond()
    {
		if(OnEventRaised != null)
		{
			OnEventRaised.Invoke();
		}
	}
}
